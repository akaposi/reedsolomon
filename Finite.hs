{-# LANGUAGE ScopedTypeVariables #-}

module Finite where

-- a class representing finite enumerable types

-- TODO: replace all usages of this with standard Haskell type classes,
--       i.e. with a combination of Enum, Real, Bounded, Integral, Ord

class Finite a where
  -- number of elements in the type, a is a dummy parameter
  size :: a -> Integer

  -- conversion to integer
  toInteg :: a -> Integer

enum :: forall a . (Num a, Finite a) => [a]
enum = map fromInteger [0..(size (undefined :: a) - 1)]
