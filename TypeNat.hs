{-# LANGUAGE TypeFamilies, EmptyDataDecls #-}

module TypeNat where

data Zero
data Succ a

-- we could remove the following definitions by using GHC 7.6's new features

type One     = Succ Zero
type Two     = Succ One
type Three   = Succ Two
type Four    = Succ Three
type Five    = Succ Four
type Six     = Succ Five
type Seven   = Succ Six
type Eight   = Succ Seven
type Nine    = Succ Eight
type Ten     = Succ Nine
type Twenty  = Sum Ten Ten
type Thirty  = Sum Twenty Ten
type Fourty  = Sum Thirty Ten
type Fifty   = Sum Fourty Ten
type Sixty   = Sum Fifty Ten
type Seventy = Sum Sixty Ten
type Eighty  = Sum Seventy Ten
type Ninety  = Sum Eighty Ten

type Eleven = Succ Ten
type Nineteen = Sum Ten Nine
type Twentyseven = Sum Twenty Seven
type Thirtyfour = Sum Thirty Four
type Sixtyseven = Sum Sixty Seven

type family Sum m n :: *
type instance Sum Zero     n = n
type instance Sum (Succ m) n = Succ (Sum m n)

natPred :: Succ a -> a
natPred = const undefined

class TypeNat a where
  natValue :: a -> Integer -- the parameter of type a here is a dummy parameter

instance TypeNat Zero where
  natValue = const 0
instance TypeNat m => TypeNat (Succ m) where
  natValue x = natValue (natPred x) + 1
