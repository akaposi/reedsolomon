{-# LANGUAGE ScopedTypeVariables #-}

module Tests where

import Control.Monad (replicateM)
import System.Random (randomRIO)

import Algebra.Eucl
import Algebra.Factor
import Algebra.Polynomial
import Finite
import ReedSolomon
import TypeNat

------------------------------------------------------
-- usage
------------------------------------------------------
-- 
-- ghci -fcontext-stack=200 Tests.hs
-- *Tests> testsAlgebra
-- *Tests> testsReedSolomon
-- *Tests> testsReedSolomonRandom
-- 
-- 
------------------------------------------------------
-- modules Algebra.*
------------------------------------------------------

-- Algebra.Polynomial
pFunctionTest :: [Bool]
pFunctionTest = [(P [-1,0,1,1::Rational]) $$ 10 == 1099, (P [0::Rational]) $$ 100 == 0, (P ([]::[Rational])) $$ 100 == 0, (P [13::Rational]) $$ 100 == 13]

-- Algebra.Factor, Algebra.Polynomial
-- testing wether the number of irreducible polynomials over F2 with degrees 0..9 are as expected ( http://oeis.org/A001037 )
testIrred :: Bool
testIrred = [sum $ map fromBool $ map irreducible $ (enumDeg n :: [P F2]) | n <- [0..9] ] == [1, 2, 1, 2, 3, 6, 9, 18, 30, 56]
  where fromBool False = 0
        fromBool True  = 1

-- Algebra.Eucl
testEuclAlg :: Eucl a => [(a,a,a)] -> Bool
testEuclAlg cases = and $ do
  p <- cases
  let (a, b, c) = p
  let (d, x, y) = head $ euclAlg a b
  return $ d `associate` c && d == x * a + y * b

euclInt :: [(Integer, Integer, Integer)]
euclInt = [(1,1,1), (10,5,5), (6,12,6), (12, 30, 6), (1324215, 32564237, 1), (2^4*3^5*5^7, 2^2*3^7*5^7, 2^2*3^5*5^7)]

euclP2 :: [(P F2, P F2, P F2)]
euclP2 = [(P[0,1,0,0,1,1]*(P[1,1,0])^2, (P[1,1,0])^3, P[1,0,1])]

{-
euclPQ :: [(P Rational, P Rational, P Rational)]
euclPQ = [((P[-1,1])^6*(P[0,1])^7,
           (P[-1,1])^8*(P[0,1])^2,
           (P[-1,1])^6*(P[0,1])^2)]
-- we cannot use this anymore since rational does not have a size instance, hence we cannot define polynomials over it
-}

euclTests :: [Bool]
euclTests = [testEuclAlg euclInt, testEuclAlg euclP2]

-- Algebra.Factor
f8Test :: Bool
f8Test = and $ [(eight !! 1) == 1] ++ map (not . (==1)) ((head eight) : (drop 2 eight))
  where eight = enum :: [F8]

testsAlgebra :: Bool
testsAlgebra = and $ pFunctionTest ++ [testIrred] ++ euclTests ++ [f8Test]

------------------------------------------------------
-- module ReedSolomon
------------------------------------------------------

alphasTest :: Bool
alphasTest = map fst (alphas 2 6 :: [(Integer, F64)]) == divisors (2^6-1)

genTest :: Bool
genTest = deg (gen (Config 2 3 4 3) :: Word F8) == 3

decodeTest :: Bool
decodeTest = (3500::P F64) == decode conf8 (encode conf8 (3500::P F64))
  where conf8 = Config 2 3 4 3

correctTest :: Bool
correctTest = correct' conf8 bCode == aCode
  where
    aCode :: P F64
    aCode = (encode conf8 3500)
    bCode = aCode + 1
    conf8 = Config 2 3 4 3

conf11 :: Config
conf11 = Config 11 1 6 4

word11 :: P F11
word11 = P [4, 5, 0, 0, 2]

err11 :: P (Fac Nat Eleven)
err11  = P [0, 0, 3, 0, 2, 0, 0, 0, 0, 0]

code11 :: P F11
code11 = encode conf11 word11

bad11 :: P F11
bad11  = code11 + err11

encodeTest :: Bool
encodeTest = unP code11 == [4,4,5,4,10,10,10,6,2]

checkTest :: Bool
checkTest = unP (check conf11 bad11) == [0,10,2,5]

genTest1 :: Bool
genTest1 = unP (gen conf11 :: P F11) == [1,8,5,3,1]

correctTest1 :: Bool
correctTest1 = correct' conf11 bad11 == code11

testsReedSolomon :: Bool
testsReedSolomon = and $ [decodeTest, correctTest, encodeTest, checkTest, genTest1, correctTest1]

correctTestRandom :: forall r c . (TypeNat r, TypeNat c) => F r c -> IO Bool
correctTestRandom _ = do
  confIndex <- randomRIO (0, length configs - 1)
  let conf = configs !! confIndex
  codeIndex <- randomRIO (0, numOfValidCodewords conf - 1)
  let code = encode conf (fromInteger codeIndex :: P (F r c))
  num <- randomRIO (0, t conf) -- we change the letters in num places
  places <- sequence [ randomRIO (0, n conf - 1) | i <- [1..num] ]
  toAdd  <- sequence [ randomRIO (1, q conf - 1) | i <- [1..num] ]
  let newCode = code + sum [ monom (fromInteger (toAdd !! (fromInteger i))) (places !! (fromInteger i))  | i <- [0..(num-1)] ]
  putStr "test "
  case correct' conf code == code of
    True  -> do
      putStrLn "passed"
      return True
    False -> do
      putStrLn "failed. Details:"
      putStrLn $ "all configs: " ++ show configs
      putStrLn $ "index of config (range): " ++ show (confIndex, (0, length configs - 1))
      putStrLn $ "config: " ++ show conf
      putStrLn $ "number of changes (range): " ++ show (num, (0, t conf))
      putStrLn $ "index of code (range): " ++ show (codeIndex, (0, numOfValidCodewords conf - 1))
      putStrLn $ "        code: " ++ show code
      putStrLn $ "changed code: " ++ show newCode
      return False

  where r = natValue (undefined::r)
        b = deg (fromInteger (natValue (undefined::c)) :: P (Z r))
        configs = [ Config r b k (n - k) | n <- divisors (r^b - 1), k <- [1..(n `div` 2)]]

correctTestRandoms :: forall r c . (TypeNat r, TypeNat c) => F r c -> Int -> IO Bool
correctTestRandoms a n = do results <- replicateM n (correctTestRandom a)
                            return $ and results

testsReedSolomonRandom :: IO Bool
testsReedSolomonRandom = do b4 <- correctTestRandoms (undefined::F4) 10
                            b8 <- correctTestRandoms (undefined::F8) 10
                            b9 <- correctTestRandoms (undefined::F9) 10
                            b16 <- correctTestRandoms (undefined::F16) 10
                            b25 <- correctTestRandoms (undefined::F25) 10
                            b27 <- correctTestRandoms (undefined::F27) 10
                            b64 <- correctTestRandoms (undefined::F64) 3
                            return $ and [b4, b8, b9, b16, b25, b27, b64]
