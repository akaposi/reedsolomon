{-# LANGUAGE PatternGuards #-}

module ReedSolomon where

import Data.Maybe (fromJust)
import Data.List
import Math.Combinatorics.Binomial (choose)

import Algebra.Eucl
import Algebra.Polynomial
import Finite

-- definition and basic properties of a configuration which describes a Reed-Solomon code

data Config = Config
                { r :: Integer
                , b :: Integer   -- underlying field (alphabet) has r^b number of elements
                , k :: Integer   -- number of letters in a word (this is what we want to encode)
                , m :: Integer } -- number of redundant letters
  deriving (Show, Eq)

-- number of letters in a codeword (including redundant letters)
n :: Config -> Integer
n conf = k conf + m conf

-- the number of different values one letter might have (number of elements in the field) (length of the alphabet)
q :: Config -> Integer
q conf = (r conf)^(b conf)

-- the distance of the code (the minimal number of different letters between valid codewords)
d :: Config -> Integer
d = (+1) . m

-- the maximal number of errors that the code can repair (one error means a different letter)
t :: Config -> Integer
t = half . d
  where half x | x `mod` 2 == 0 = x `div` 2 - 1
               | otherwise      = (x - 1) `div` 2

numOfAllCodewords :: Config -> Integer
numOfAllCodewords conf = (q conf)^(n conf)

numOfValidCodewords :: Config -> Integer
numOfValidCodewords conf = (q conf)^(k conf)

-- number of codewords which are further than t distance away from a valid codeword
numOfInvalidCodewords :: Config -> Integer
numOfInvalidCodewords conf = numOfAllCodewords conf - numOfValidCodewords conf *
  sum [ choose (n conf) errorCount * (q conf-1)^errorCount | errorCount <- [0..(t conf)] ]
-- we substract the number of correctable codewords from the number of all codewords

-- datatypes representing codes

type Word a     = P a -- words representing encodable information
type Codeword a = P a -- codewords which include the redundant bits
type Synd a     = P a -- syndrome

-- the code itself

-- find an element which has ordo i
findOrd :: (Eq a, Num a, Finite a) => Integer -> Integer -> a
findOrd b i = fromJust $ find ((==i) . ord) (map fromInteger [0..]) -- TODO: exclude usage of lists
  where
    ord 0 = -1
    ord n = 1 + toInteger (fromJust $ findIndex (==1) (iterate (*n) n))

-- alphas r b provides an element in F_r^b having ordo i for all possible i-s
-- the elements are given back paired with their degree
alphas :: (Eq a, Num a, Finite a) => Integer -> Integer -> [(Integer, a)]
alphas r b = map (\i -> (i, findOrd b i)) (divisors (r^b - 1))

-- provides an element in F_r^b having ordo (k+m)
alpha :: (Eq a, Num a, Finite a) => Config -> a
alpha (Config _ b k m) = findOrd b (k + m)

-- the generator (multiplying a word with this gives a valid code)
gen :: (Eq a, Num a, Finite a) => Config -> P a
gen conf = product [ P [-(a^i), 1] | i <- [1..(m conf)] ]
  where a = alpha conf

-- the essence of the Reed-Solomon algorithm

encode :: (Eq a, Num a, Finite a) => Config -> Word a -> Codeword a
encode conf = (*) (gen conf)

check :: (Eq a, Num a, Finite a) => Config -> Codeword a -> Synd a
check conf@(Config _ b k m) u = P [ sum [ a^(i*j) * (u ! j) | j <- [0..(n-1)] ] | i <- [1..m] ]
  where n = k + m
        a = alpha conf

decode :: (Eq a, Fractional a, Finite a) => Config -> Codeword a -> Word a
decode conf c = u
  where g      = gen conf
        (u, _) = divmod c g

-- error correction

-- returns the number of errors and the corrected polynomial
correct :: (Eq a, Fractional a, Finite a) => Config -> Codeword a -> Maybe (Integer, Codeword a)
correct conf v | s == 0                    = Just (0, v)
               | Nothing <- maybe_rxy      = Nothing
               | (y $$ 0) == 0             = Nothing
               | (check conf (v - e)) == 0 = Just (sum [ 1 | j <- [0..(k'+m'-1)] , ej j /= 0 ], v - e)
               | otherwise                 = Nothing
  where s = check conf v -- the syndrome
        (Config _ _ k' m') = conf
        l = euclAlg (monom 1 m') s -- the principal coeff. of s is nonzero if s is nonzero
        maybe_rxy = find (\(r',_,_) -> deg r' <= half m') . reverse $ l
        (r',_,y) = fromJust maybe_rxy
        half m | even m    = m `div` 2 - 1
               | otherwise = (m - 1) `div` 2
        pL = y  * embed (recip $ y $$ 0)
        pE = r' * embed (recip $ y $$ 0)
        pLj j = fst $ divmod pL (P [1, -(a^j)])
        a = alpha conf
        ej j | (pL $$ amj) /= 0 = 0
             | otherwise        = (pE $$ amj) * (recip $ a^j * (pLj j $$ amj)) -- the error elements, j = 0..(n-1)
          where amj = recip $ a^j
        e = P [ ej j | j <- [0..(k'+m'-1)]] -- the error vector, c = v - e

correct' :: (Eq a, Fractional a, Finite a) => Config -> Codeword a -> Codeword a
correct' conf v | Just (_, v') <- correct conf v = v'
                | otherwise                      = error "we were unable to correct the errors"
