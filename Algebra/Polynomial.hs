{-# LANGUAGE ScopedTypeVariables #-}

module Algebra.Polynomial
  ( P(..)
  , ($$)
  , monom
  , embed
  , (!)
  , lift
  , enumDegs
  , enumDeg
  , irreducible
  , irreducibleDeg
  , irreducibleIndexDeg
  ) where

import Data.Maybe (fromJust)
import Data.List

import Algebra.Eucl
import Finite

-- polynomials over finite types

-- polynomials are lists of coefficients starting with the least degree coeff.

data P a = P { unP :: [a] }

-- converting a polynomial into a polynomial function
($$) :: (Eq a, Num a) => P a -> a -> a
(P [])     $$ a = 0
(P [c])    $$ a = c
(P (c:cs)) $$ a = c + (sum $ zipWith (\i c -> c*a^i) [1..] cs)

monom :: (Eq a, Num a) => a -> Integer -> P a
monom coeff exponent = P $ genericReplicate exponent 0 ++ [coeff]

-- embedding a value into a one-degree polynomial
embed :: (Eq a, Num a) => a -> P a
embed a | a == 0    = P []
        | otherwise = P [a]

-- getting the i^th coefficient
(!) :: Num a => P a -> Integer -> a
_          ! n | n < 0 = error "negative index"
(P (f:_ )) ! 0 = f
(P (_:fs)) ! n = P fs ! (n - 1)
(P _)      ! _ = 0

lift :: (Eq a, Num a, Finite a) => P a -> P Integer
lift = P . map toInteg . unP

removeTrailing0s :: (Eq a, Num a) => P a -> P a
removeTrailing0s = P . reverse . dropWhile (==0) . reverse . unP

instance Show a => Show (P a) where
  show x = "P" ++ (show . unP) x

instance (Eq a, Num a) => Eq (P a) where
  f == g = unP (removeTrailing0s f) == unP (removeTrailing0s g)

plus :: (Eq a, Num a) => P a -> P a -> P a
P (f:fs) `plus` P (g:gs) = P $ f+g : (unP $ P fs `plus` P gs)
pfs      `plus` P []     = pfs
P []     `plus` pgs      = pgs

times :: (Eq a, Num a) => P a -> P a -> P a
P (f:fs) `times` P (g:gs) = P $ f*g : (unP $ ((embed f) `times` P gs) `plus` (P fs `times` P (g:gs)))
_        `times` _        = P []

instance (Eq a, Num a, Finite a) => Num (P a) where
  x + y = removeTrailing0s $ x `plus` y
  x * y = removeTrailing0s $ x `times` y

  abs    = error "abs undefined for Num (P a)"
  signum = error "signum undefined for Num (P a)"
  negate = P . map negate . unP

  -- converts x (a positive integer) into the x^th polynomial over a
  fromInteger x = P $ map fromInteger $ toPol x []
    where
      toPol :: Integer -> [Integer] -> [Integer]
      toPol 0 xs = xs
      toPol m xs = toPol (m `div` len) (xs ++ [m `mod` len])
      len        = size (undefined :: a)

instance (Eq a, Fractional a, Finite a) => Eucl (P a) where
  divmod f g | g == 0        = error "division by null"
  divmod f g | deg f < deg g = (0, f)
             | otherwise     = (embed (f_n * recip g_k) * monom 1 (n - k) + q', r') -- g_k is nonnull
    where n   = deg f
          k   = deg g
          f_n = f ! n
          g_k = g ! k
          f'  = f - embed (f_n * recip g_k) * g * monom 1 (n - k) -- g_k is nonnull
          (q', r') = divmod f' g

  deg = pred . toInteger . length . dropWhile (==0) . reverse . unP

-- TODO: replace the functions below with more efficient ones

-- enumerate all polynomials between degree m and n
enumDegs :: (Eq a, Fractional a, Finite a) => Integer -> Integer -> [P a]
enumDegs m n = takeWhile ((<=n).deg) (dropWhile ((<m).deg) enum)
  where enum = map fromInteger [0..]

-- enumerate all polynomials wih degree n
enumDeg :: (Eq a, Fractional a, Finite a) => Integer -> [P a]
enumDeg n = enumDegs n n

-- testing wether a polynomial is irreducible
irreducible :: (Eq a, Fractional a, Finite a) => P a -> Bool
irreducible p = all ((/=0) . snd . divmod p) ps
  where n  = deg p
        ps = enumDegs 1 (n `div` 2)
        -- we only divide with polynomials with degree at least 1 (0-degree polynomials are units, the -1 degree polynomial is zero)
        -- we only have to look at polynomials until degree n/2, since the degrees add up when summing polynomials

-- provide an irreducible polynomial of degree n
irreducibleDeg :: (Eq a, Fractional a, Finite a) => Integer -> P a
irreducibleDeg n = fromJust $ find irreducible (enumDeg n)

-- provide an irreducible polynomial of degree n and it's index in the list of all polynomials
irreducibleIndexDeg :: (Eq a, Fractional a, Finite a) => Integer -> (P a, Integer)
irreducibleIndexDeg n = fromJust $ find (\(p,_) -> deg p == n && irreducible p) $ map (\i->(fromInteger i, i)) [0..]
