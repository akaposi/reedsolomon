module Algebra.Eucl
  ( Eucl(..)
  , divisorOf
  , associate
  , euclAlg
  , divisors
  ) where

-- Euclidean ring

class (Eq a, Num a) => Eucl a where
  divmod :: a -> a -> (a, a)
  deg    :: a -> Integer

divisorOf :: Eucl a => a -> a -> Bool
x `divisorOf` y = (==0) $ snd $ divmod x y

associate :: Eucl a => a -> a -> Bool
associate x y = x `divisorOf` y && y `divisorOf` x

instance Eucl Integer where
  divmod a b = (a `div` b, a `mod` b)
  deg = abs

-- the extended Euclidean algorithm, returns a list of triples (r, x, y) where r=ax+by 
-- and the first element is (d, x, y) where d is the GCD
euclAlg :: Eucl a => a -> a -> [(a, a, a)]
euclAlg a b = step [(b, 0, 1), (a, 1, 0)]
  where
    step :: (Eq a, Num a, Eucl a) => [(a, a, a)] -> [(a, a, a)]
    step ((r_sn,x_sn,y_sn):(r_n,x_n,y_n):aaas) | r_sn == 0 = (r_n,x_n,y_n):aaas
                                               | otherwise = step $ (r_ssn,x_ssn,y_ssn):(r_sn,x_sn,y_sn):(r_n,x_n,y_n):aaas
      where (q_sn, r_ssn) = divmod r_n r_sn
            x_ssn = x_n - q_sn * x_sn
            y_ssn = y_n - q_sn * y_sn

divisors :: (Enum a, Eucl a) => a -> [a]
divisors n = [ i | i <- [1..n] , n `mod` i == 0 ]
  where x `mod` y = let (_, z) = divmod x y in z
