module F where

-- how would we implement factor rings in agda

postulate
  P : Set → Set -- polynomials
  _+'_ : {A : Set} → P A → P A → P A

data F {A : Set} : P A → Set where
  const : {f : P A} (g : P A) → F f

postulate
  normalize : {A : Set} (f : P A) → P A → P A

_+_ : {A : Set} {f : P A} → F f → F f → F f
const {f} g + const {.f} h = const {_} {f} (normalize f (g +' h))

-- we should use instance arguments to use the same identifier for + and +'
