p0 <- .95
b <- 3
k <- 5
m <- 4

# feltetel: k + m = n | (2^b - 1)

# jelolt minta eseten a helyes kiolvasas valoszinusege:

# 0 hibat tudunk kijavitani:
p0^(b*k)
# 1 hibat tudunk kijavitani (m = 2):
p0^(b*(k+2)) + choose(k+2, 1) * (1-p0^b)^1 * p0^(b*(k+1))
#              ^ enny b-esben lehet hiba
#                               ^legalabb egy hiba van egy b-esben
#                                            ^a tobbiben nincs hiba
# 2 hivat tudunk kijavitani (m = 4):
p0^(b*(k+4)) + choose(k+4, 1) * (1-p0^b)^1 * p0^(b*(k+3)) +
               choose(k+4, 2) * (1-p0^b)^2 * p0^(b*(k+2))

# altalanosan:
P.kiolvashato <- function(p0, b, k, m) {
  if (m %% 2 != 0) stop("m should be even")
  
  ret <- 0
  javithato <- m/2
  for (i in 0:javithato)
    ret <- ret + choose(k + m, i) * (1-p0^b)^i * p0^(b*(k + m - i))

  return(ret)
}

P.kiolvashato(p0, b, k, 4)


# kaptunk egy kodszot. annak val.sege, hogy helyesen olvastuk ki,
# a fenti p. 1-p annak a val.sege, hogy nem tudjuk helyesen kiolvasni,
# ez az, amikor azt hisszuk, hogy jol olvastuk ki, de megsem.

# kerdes: ad -e tovabbi informaciot az, ha egy kiolvasas utan
# figyelembe vesszuk, hogy mennyi hibas b-est talaltunk


# l db hibat talaltunk (l db hibas b-est). mennyi a val.sege, hogy ennyi, vagy ennel
# tobb hiba jon letre es jol javitottunk? (tehat a hibak szama kevesebb, mint m/2)

P.ennyivagytobbhiba <- function(p0, b, k, m, l) {
  if (m %% 2 != 0) stop("m should be even")
  if (l > m / 2)   stop("l (the number of errors) should be less or equal to m/2 (the number of recoverable errors)")
  
  ret <- 0
  for (i in l:(m/2))
    ret <- ret + choose(k + m, i) * (1 - p0^b)^i * p0^(b*(k + m - i))

  return(ret)
}

# P.ennyivagytobbhiba(p0, b, k, m, 0) == P.kiolvashato(p0, b, k, m)

# annak val.sege, hogy rosszul olvastuk ki a kodot (ez nem fugg a hibak szamatol)
# P.rosszulolvastunk(...) == 1-P.kiolvashato(...)

P.rosszulolvastunk <- function(p0, b, k, m) {
  if (m %% 2 != 0) stop("m should be even")

  ret <- 0

  for (i in (m/2+1):(k+m))
    ret <- ret + choose(k + m, i) * (1 - p0^b)^i * p0^(b*(k + m - i))
  return(ret)
}

# ha l hibat tapasztalunk, mennyi a val.sege, hogy egy hamisito random
# probalkozas alapjan talal egy kodot, melyben l vagy ennel kevesebb hiba van?
# mas megfogalmazassal: max. ennyi hiba van?

P.hamisit <- function(p0, b, k, m, l) {
  if (m %% 2 != 0) stop("m should be even")
  if (l > m / 2)   stop("l (the number of errors) should be less or equal to m/2 (the number of recoverable errors)")

  ret <- 0

  for (i in 0:l)
    ret <- ret + (2^(b*k) * choose(k+m, i) * 2^(b*l)) / 2^(b * (k + m))
  return(ret)
}

P.minden <- function(p0, b, k, m, l) {
  if (m %% 2 != 0) stop("m should be even")
  if (l > m / 2)   stop("l (the number of errors) should be less or equal to m/2 (the number of recoverable errors)")

  print(paste("i)   P(valid a kod, de rosszul olvastuk)  = ", P.rosszulolvastunk(p0,b,k,m)))
  print(paste("ii)  P(valid a kod, es min. ennyi a hiba) = ", P.ennyivagytobbhiba(p0,b,k,m,l)))
  print(paste("iii) P(hamis a kod, es max. ennyi a hiba) = ", P.hamisit(p0,b,k,m,l)))
  
}





# teszt, hogy jol szamoltam -e a hibas szavakat:

teszt <- function(b, k, m) {
  if ((2^b - 1) %% (k + m) != 0) stop("(k+m) | (2^b-1) is required")
  if (m %% 2 != 0)               stop("m should be even")

  x <- 0

  for (i in 0:(m/2))
    x <- x + choose(k + m, i) * (2^b - 1)^i

  return(c(x, 2^(b*m)))
}

