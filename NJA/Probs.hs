{-# LANGUAGE ScopedTypeVariables, PatternGuards #-}

module NJA.Probs where

import Control.Monad (forM_)
import Math.Combinatorics.Binomial (choose)

import Algebra.Eucl (divisors)
import Algebra.Factor
import Algebra.Polynomial (P, irreducibleIndexDeg)
import NJA.Convert
import NJA.Perm
import ReedSolomon
import TypeNat

-- calculating probabilities for reading of Reed-Solomon codes and listing configs

-- all the functions in this file only work with configs where r=2

type Prob = Double

---------------------------------------------------------------------------------------------
-- Functions helping inference about a given codeword (whether it is valid, fake etc.)
---------------------------------------------------------------------------------------------

-- goodRead p c l is the probability that given a valid codeword at least l failures happen
goodRead :: Prob -> Config -> Integer -> Prob
goodRead p c@(Config _ b k m) l = 1 - sum [ fromIntegral (choose (k+m) i) * (1-p^b)^i * p^(b*(k+m-i)) | i <- [0..(l-1)] ]

-- an upper limit of the probability that the (with l failures) obtained codeword arose from another valid codeword,
-- that is more than t distance away from the obtained codeword
fromOtherValidCode :: Prob -> Config -> Integer -> Prob
fromOtherValidCode p c@(Config _ b k m) l = 1 - sum [ fromIntegral (choose n i) * (1-p^b)^i * p^(b*(n-i)) | i <- [0..(m-l)] ]
  where n = m + k

-- fake c l is the probability that by a random choice we can achieve a codeword with no more than l failures
-- (statistical inference...)
fake :: Config -> Integer -> Prob
fake c@(Config _ b k m) l = sum [ (2.0^(b*k) * fromIntegral (choose n i) * (2^b-1)^i) / (2.0^(b*n)) | i <- [0..l] ]
  where n = k + m

-- badRead is the probability that we won't be able to read a valid codeword using this configuration
-- this means that at least t+1 failures have happened
badRead :: Prob -> Config -> Prob
badRead p c@(Config _ b k m) = 1 - sum [ fromIntegral (choose n i) * (1-p^b)^i * p^(b*(n-i)) | i <- [0..(t c)] ]
  where n = k + m

-- propInvalids conf gives the proportion of invalid codewords (which cannot be corrected)
propInvalids :: Config -> Prob
propInvalids conf = fromIntegral (numOfInvalidCodewords conf) / fromIntegral (numOfAllCodewords conf)

-- badRead and propFakes describe how well a Config behaves in general (for evaluating Configs when planning)
-- when reading a code, we provide the probabilities given by:
-- a) goodRead
-- b) fromOtherValidCode
-- c) fake
-- when reading a code with more that t failures (i.e. when the error correction is unsuccessful) we give the probabilities:
-- a+b) badRead
-- c) propInvalids

-- we'd like to know the proportion of words that are counted as fakes: we start counting them when the probability
-- of being fake is higher than the probability of being a goodRead
-- propFakes p c returns this value
propFakes :: Prob -> Config -> Prob
propFakes p c@(Config _ b k m) =
  fromIntegral (sum [ 2^(b*k) * choose (k+m) l * (2^b-1)^l | l <- [0..(t c)] , goodRead p c l < fake c l ] + theMissed) / fromIntegral (numOfAllCodewords c)
  where theMissed = numOfAllCodewords c - sum [ 2^(b*k) * choose (k+m) l * (2^b-1)^l | l <- [0..(t c)] ]

-- putting the above pieces together
-- note that argument p = p0^(conf b), and also that cw should not be permuted
inference :: TypeNat c => Prob -> Config -> Codeword (F Two c) -> (Maybe (Codeword (F Two c)), String)
inference p conf cw
  | Just (l, cw') <- correct conf cw = ( Just cw',
                                         concat
                                           [ "probabilities:\n"
                                           , "  valid codeword: ", show (goodRead p conf l), "\n"
                                           , "  bad correction: ", show (fromOtherValidCode p conf l), "\n"
                                           , "  fake: ", show (fake conf l), "\n" ]
                                       )
  | otherwise                        = ( Nothing,
                                         concat
                                           [ "probabilities:\n"
                                           , "  valid codeword: ", show (badRead p conf), "\n"
                                           , "  fake: ", show (propInvalids conf), "\n" ]
                                       )

-- a higher level interface to inference
-- doing hexadecimal<->codeword conversion and permutation
-- the given permutation is the encoding permutation and works on the binary level
inferenceHex :: forall c . TypeNat c => c -> Prob -> Config -> [Integer] -> Hex -> (Maybe Hex, String)
inferenceHex _ p conf perm x = (fmap (binaryToHex . permute perm . wordToBinary conf) cw, info)
  where
    (cw, info) = inference p conf
                           ( binaryToWord conf
                           $ permute (invert perm)
                           $ supplZeros (n conf * b conf)
                           $ hexToBinary x
                           :: Codeword (F Two c))

-- same as inferenceHex, but returns the corrected word (without
-- redundant parts) in integer format, not the corrected codeword
inferenceWord :: forall c . TypeNat c => c -> Prob -> Config -> [Integer] -> Hex -> (Maybe Integer, String)
inferenceWord _ p conf perm x = (fmap (wordToInt . decode conf) cw, info)
  where
    (cw, info) = inference p conf
                           ( binaryToWord conf
                           $ permute (invert perm)
                           $ supplZeros (n conf * b conf)
                           $ hexToBinary x
                           :: Codeword (F Two c))

---------------------------------------------------------------------------------------------
-- Functions helping the design of codes
---------------------------------------------------------------------------------------------

-- to design a code, we need to adjust the following numeric values:
-- 1. numOfAllValidCodewords
-- 2. badRead
-- 3. propFakes

enumerateConfigs :: [Config]
enumerateConfigs = [ Config 2 b k (n - k) | b <- [1..], n <- divisors (2^b - 1), k <- [1..n] ]

-- prints the properties of the given configurations
printConfigInfos :: Prob -> [Config] -> IO ()
printConfigInfos p0 confs = do
  putStrLn $ concat
      [ "config\t"
      , "n\t"
      , "d\t"
      , "t\t"
      , "number of bits\t"
      , "number of valid codewords - 1\t"
      , "prob. of not being able to read a valid codeword\t"
      , "ratio of codewords that are considered fake\t"
      , "c" ]
  forM_ confs $ \conf ->
    putStrLn $ concat 
      [ drop 7 $ show (conf), "\t"
      , show $ n conf, "\t"
      , show $ d conf, "\t"
      , show $ t conf, "\t"
      , show $ n conf * b conf, "\t"
      , show $ numOfValidCodewords conf - 1, "\t"
      , show $ badRead (p0 ^ b conf) conf, "\t"
      , show $ propFakes (p0 ^ b conf) conf, "\t"
      , show $ snd (irreducibleIndexDeg (b conf) :: (P (Z Two), Integer)) ]
  return ()

filterConfigs :: Prob -> Integer -> Prob -> Prob -> Integer -> [Config]
filterConfigs p0 count maxBadRead minPropFakes maxMaterialCount =
  filter (\conf -> numOfValidCodewords     conf >  count
                && badRead (p0 ^ b conf)   conf <= maxBadRead
                && propFakes (p0 ^ b conf) conf >= minPropFakes
                && b conf * n conf              <= maxMaterialCount
         )
         enumerateConfigs

---------------------------------------------------------------------------------------------
-- Functions listing codewords for building databases
---------------------------------------------------------------------------------------------

-- enumerate all codewords in conf
enumerateCodewords :: forall c . TypeNat c => c -> Config -> [[F2]]
enumerateCodewords _ conf = [ wordToBinary conf (encode conf (intToWord i :: Word (F Two c)))
                              | i <- [1..(numOfValidCodewords conf - 1)] ]

-- enumerate first count codewords in conf in binary and hexadecimal form (only the permuted versions)
printCodewords :: forall c . TypeNat c => c -> Config -> [Integer] -> Int -> IO ()
printCodewords dummy conf perm count = do
  putStrLn "number\tbinary codeword\thexadecimal codeword"
  forM_ (zip [1..count] $ enumerateCodewords dummy conf) $ \(i, cw) ->
    putStrLn $ concat
      [ show i, "\t"
      , concat $ map show $ permute perm cw, "\t"
      , binaryToHex $ permute perm cw ]

-- encode a code given as an integer number as a hexadecimal (permuted) number
encodeHex :: forall c . TypeNat c => c -> Prob -> Config -> [Integer] -> Integer -> Hex
encodeHex _ p conf perm i
  | i >= numOfValidCodewords conf = undefined
  | otherwise = 
      binaryToHex $ permute perm $ wordToBinary conf 
                                                (encode conf (fromInteger i) :: Codeword (F Two c))
