{-# LANGUAGE ScopedTypeVariables #-}

module NJA.MonteCarlo where

import System.Random (randomRIO)

import Algebra.Factor
import NJA.Convert
import NJA.Probs
import ReedSolomon
import TypeNat

-----------------------------------
-- Monte Carlo simulation for 2.1.e)
-----------------------------------

-- pointwise comparison of two binary numbers
lte :: [F2] -> [F2] -> Bool
lte []     []     = True
lte (1:as) (0:bs) = False
lte (_:as) (_:bs) = lte as bs

bor :: [F2] -> [F2] -> [F2]
bor [] [] = []
bor (1:as) (_:bs) = 1:(bor as bs)
bor (0:as) (1:bs) = 1:(bor as bs)
bor (0:as) (0:bs) = 0:(bor as bs)

-- taken two random codes in conf, how many of all codes are lower than or equal to their bitwise or product?

monteCarlo :: forall c . TypeNat c => c -> Config -> IO Integer
monteCarlo c conf = do
  i1 <- randomRIO (1, (numOfValidCodewords conf - 1))
  i2 <- randomRIO (1, (numOfValidCodewords conf - 1))
  let j1 = wordToBinary conf $ encode conf (intToWord i1 :: Word (F Two c))
  let j2 = wordToBinary conf $ encode conf (intToWord i2 :: Word (F Two c))
  let j12 = bor j1 j2
--  print j12
  return $ fromIntegral $ length $ filter (\x -> lte x j12) $ enumerateCodewords c conf

monteCarlos2 :: TypeNat c => c -> Integer -> Config -> IO [Integer]
monteCarlos2 c 0 conf = return []
monteCarlos2 c n conf = do
  num <- monteCarlo c conf
  cs  <- monteCarlos2 c (n-1) conf
  return $ num : cs

monteCarlo3 :: forall c . TypeNat c => c -> Config -> IO Integer
monteCarlo3 c conf = do
  i1 <- randomRIO (1, (numOfValidCodewords conf - 1))
  i2 <- randomRIO (1, (numOfValidCodewords conf - 1))
  i3 <- randomRIO (1, (numOfValidCodewords conf - 1))
  let j1 = wordToBinary conf $ encode conf (intToWord i1 :: Word (F Two c))
  let j2 = wordToBinary conf $ encode conf (intToWord i2 :: Word (F Two c))
  let j3 = wordToBinary conf $ encode conf (intToWord i3 :: Word (F Two c))
  let j123 = bor (bor j1 j2) j3
--  print j123
  return $ fromIntegral $ length $ filter (\x -> lte x j123) $ enumerateCodewords c conf

monteCarlos3 :: TypeNat c => c -> Integer -> Config -> IO [Integer]
monteCarlos3 c 0 conf = return []
monteCarlos3 c n conf = do
  num <- monteCarlo3 c conf
  cs  <- monteCarlos3 c (n-1) conf
  return $ num : cs
