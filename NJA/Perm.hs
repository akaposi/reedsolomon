module NJA.Perm where

import Data.List (genericIndex, sortBy)
import System.Random (randomRIO)
import ReedSolomon (Config(Config))

-- provides a random permutation with the Fisher-Yates (Knuth) algorithm
-- this permutes on the binary level
permutation :: Config -> IO [Integer]
permutation (Config _ b k m) = perm 0 [0..b * (k + m) - 1]
  where
    perm :: Integer -> [Integer] -> IO [Integer]
    perm i ls | i < b*(k+m)-1 = do j <- randomRIO (0, b*(k+m)-1)
                                   let ls' = swap (fromIntegral i) (fromIntegral j) ls
                                   perm (i+1) ls'
              | otherwise = return ls
    swap i j ls = [ change k x | (k, x) <- zip [0..length ls - 1] ls ]
      where change k x | k == i    = ls !! j
                       | k == j    = ls !! i
                       | otherwise = x

-- applies a permutation to a list
permute :: [Integer] -> [a] -> [a]
permute perm xs = [ xs `genericIndex` i | i <- perm ]

-- invert a given permutation
invert :: [Integer] -> [Integer]
invert perm = map fst $ sortBy (\(_,x)(_,y)->compare x y) $ zip [0..] perm
