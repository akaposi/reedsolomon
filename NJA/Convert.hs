{-# LANGUAGE ScopedTypeVariables #-}

module NJA.Convert where

import Data.List

import Algebra.Eucl
import Algebra.Factor
import Algebra.Polynomial
import Finite
import ReedSolomon
import TypeNat

-- in this file it is assumed that r = 2 in all the Configs

-- add zeros in the end until the sequence reaches the given length (n)
supplZeros :: Num a => Integer -> [a] -> [a]
supplZeros n bs = bs ++ genericReplicate (n  - genericLength bs) 0


intToBinary :: Integer -> [F2]
intToBinary 0 = []
intToBinary n = fromInteger (n `mod` 2) : intToBinary (n `div` 2)

binaryToInt :: [F2] -> Integer
binaryToInt bs = binaryToInt' bs 0
  where
    binaryToInt' []     _ = 0
    binaryToInt' (0:bs) n = binaryToInt' bs (n+1)
    binaryToInt' (1:bs) n = 2^n + binaryToInt' bs (n+1)

type Hex = String

hexToBinary :: Hex -> [F2]
hexToBinary []     = []
hexToBinary (h:hs) = (supplZeros 4 $ intToBinary $ fromHex h) ++ hexToBinary hs
  where
    fromHex h | '0' <= h && h <= '9' = read $ h:[]
              | h == 'A' = 10
              | h == 'B' = 11
              | h == 'C' = 12
              | h == 'D' = 13
              | h == 'E' = 14
              | h == 'F' = 15
              | otherwise = 0  -- TODO: should return Maybe

binaryToHex :: [F2] -> Hex
binaryToHex []          = []
binaryToHex (a:[])      = binaryToHex [a,0,0,0]
binaryToHex (a:b:[])    = binaryToHex [a,b,0,0]
binaryToHex (a:b:c:[])  = binaryToHex [a,b,c,0]
binaryToHex (a:b:c:d:r) = toHex (binaryToInt [a,b,c,d]) : binaryToHex r
  where
    toHex i | i < 10  = head $ show i
            | i == 10 = 'A'
            | i == 11 = 'B'
            | i == 12 = 'C'
            | i == 13 = 'D'
            | i == 14 = 'E'
            | i == 15 = 'F'

hexToInt :: Hex -> Integer
hexToInt hs = hexToInt' hs 0
  where
    hexToInt' []     _ = 0
    hexToInt' (h:hs) n = fromHex h * 16^n + hexToInt' hs (n+1)
    fromHex h | '0' < h && h <= '9' = read (h:[])
              | h == 'A' = 10
              | h == 'B' = 11
              | h == 'C' = 12
              | h == 'D' = 13
              | h == 'E' = 14
              | h == 'F' = 15
              | otherwise = 0  -- TODO: should return Maybe

binaryToGrouped :: Config -> [F2] -> [Integer]
binaryToGrouped _                   [] = []
binaryToGrouped conf@(Config _ b _ _) bs = binaryToInt (genericTake b bs) : binaryToGrouped conf (genericDrop b bs)

groupedToBinary :: Config -> [Integer] -> [F2]
groupedToBinary (Config _ b _ _) = concatMap (supplZeros b . intToBinary)

groupedToWord :: TypeNat c => [Integer] -> Word (F Two c)
groupedToWord = P . map fromInteger

-- this adds zeros in the end to match the length given in the configuration
wordToGrouped :: TypeNat c => Config -> Word (F Two c) -> [Integer]
wordToGrouped conf = supplZeros (n conf) . map toInteg . unP

intToWord :: TypeNat c => Integer -> Word (F Two c)
intToWord = fromInteger

wordToInt :: forall c . TypeNat c => Word (F Two c) -> Integer
wordToInt p = lift p $$ (2^b)
  where b = deg (fromInteger (natValue (undefined :: c)) :: P F2)

wordToBinary :: forall c . TypeNat c => Config -> Word (F Two c) -> [F2]
wordToBinary conf = groupedToBinary conf . wordToGrouped conf

binaryToWord :: TypeNat c => Config -> [F2] -> Word (F Two c)
binaryToWord conf = groupedToWord . binaryToGrouped conf

wordToHex :: TypeNat c => Config -> Word (F Two c) -> Hex
wordToHex conf = binaryToHex . wordToBinary conf

hexToWord :: TypeNat c => Config -> Hex -> Word (F Two c)
hexToWord conf = binaryToWord conf . hexToBinary
